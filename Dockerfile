FROM python:3.7-slim

ENV PATH="/app/bin:${PATH}" \
    PYTHONPATH=/app/src 

WORKDIR /app

COPY . /app/

RUN pip3 install -r /app/conf/app/requirements.txt


ENTRYPOINT ["/bin/bash", "-c"]

