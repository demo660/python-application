# kustomize

## kustomization explanation

this main `kustomization` file implements ./base without overrding the namespace

```
.
├── README.md               - this file
├── base
│   ├── deployment.yaml     - deployment of sample-app
│   ├── ingress.yaml        - ingress resource for sample-app
│   ├── kustomization.yaml
│   └── svc.yaml            - service resource for sample-app
├── kustomization.yaml
├── multi
│   ├── README.md
│   ├── deployment.yaml     - deploayment patch of sample-app
│   └── kustomization.yaml
└── single
    ├── README.md
    └── kustomization.yaml
```