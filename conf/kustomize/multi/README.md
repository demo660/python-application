# multi

## kustomization explanation

1. Set namespace

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

namespace: demo
...
```

1. Override number of `replicas`

- Get the base:
```yaml
resources:
  - ../base
```

- Override the number of `replicas`
```yaml
patchesStrategicMerge:
  - deployment.yaml
```

- `Deployment.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "sample-app-web"
  labels:
    team: "Tikal"
    app: "sample-app"
    component: "web"
spec:
  replicas: 2
```